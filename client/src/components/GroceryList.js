import React, { Suspense } from 'react';
import { Spinner } from 'react-bootstrap';

import { Await, useLoaderData } from 'react-router-dom';

function GroceryList () {
  const { data } = useLoaderData();

  return (
    <Suspense fallback={<Spinner />}>
      <Await resolve={data}>
        {(data) => (
          <GroupedGroceryList data={data} />
        )}
      </Await>
    </Suspense>
  );
}

function GroupedGroceryList ({ data }) {
  const categoryById = data.categories.reduce((previousValue, currentValue) => {
    return { ...previousValue, [currentValue.id]: currentValue };
  }, {});
  const foodsByCategory = data.foods.reduce((previousValue, currentValue) => {
    const { category } = currentValue;
    if (category in previousValue) {
        previousValue[category].push(currentValue);
    } else {
        previousValue[category] = [currentValue];
    }
    return previousValue;
  }, {});

  return (
    <div>
      {
        Object
          .keys(foodsByCategory)
          .map(category => parseInt(category))
          .map(category => <GroceryListItemGroup categoryName={categoryById[category].name} foods={foodsByCategory[category]} key={category} />)
      }
    </div>
  );
}

function GroceryListItemGroup ({ categoryName, foods }) {
  return (
    <div>
      <p>
        <strong>{categoryName}</strong>
      </p>
      <ul>
        {foods.map(food => <GroceryListItem food={food} key={food.id} />)}
      </ul>
    </div>
  );
}

function GroceryListItem ({ food }) {
  return <li>{food.name}</li>
}

export default GroceryList;
