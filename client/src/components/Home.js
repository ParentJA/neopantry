import React, { useState } from 'react';

import axios from 'axios';
import { Formik } from 'formik';
import { Button, Card, Form } from 'react-bootstrap';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import { Link, useNavigate } from 'react-router-dom';

function Home ({ isLoggedIn }) {
  const [isLoading, setLoading] = useState(false);
  const [options, setOptions] = useState([]);
  
  const navigate = useNavigate();

  const onSubmit = async (values, actions) => {
    const params = encodeURIComponent(values.query);
    navigate(`/search/${params}`);
  };

  const recipeSearchWord = async (query) => {
    if (query.length < 3) {
      setLoading(false);
      setOptions([]);
    } else {
      setLoading(true);
      try {
        const response = await axios({
          method: 'get',
          url: '/api/v1/recipes/recipe-search-words/',
          params: {
            query: query,
          }
        });
        setOptions(response.data);
      } catch(error) {
        console.error(error);
        setOptions([]);
      } finally {
        setLoading(false);
      }
    }
  };

  return (
    <>
      <Formik
        initialValues={{
          query: '',
        }}
        onSubmit={onSubmit}
      >
        {({
          handleChange,
          handleSubmit,
          setFieldValue,
          values,
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Group controlId='query'>
              <Form.Label>Find a recipe</Form.Label>
              <AsyncTypeahead
                filterBy={() => true}
                id="query"
                isLoading={isLoading}
                labelKey="word"
                name="query"
                onChange={selected => {
                  const value = selected.length > 0 ? selected[0].word : '';
                  setFieldValue('query', value);
                }}
                onInputChange={value => setFieldValue('query', value)}
                onSearch={recipeSearchWord}
                options={options}
                placeholder="Enter a search term (e.g. spicy)"
                type="text"
                value={values.query}
              />
              <Form.Text className='text-muted'>
                Searches for query in name and description.
              </Form.Text>
            </Form.Group>
            <div className='d-grid mb-3'>
              <Button
                type='submit' 
                variant='primary'
              >Search
              </Button>
            </div>
          </Form>
        )}
      </Formik>
      <Card.Text className='text-center'>
        <Link to='/search'>Advanced search</Link>
      </Card.Text>
    </>
  );
}

export default Home;
