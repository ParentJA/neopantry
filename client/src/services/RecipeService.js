import axios from 'axios';

export const getRecipe = async (id) => {
  const url = `/api/v1/recipes/${id}/`;
  return axios.get(url);
};

export const getRecipeGroceryList = async (recipe_id) => {
  const url = `/api/v1/recipes/${recipe_id}/grocery-list/`;
  return axios.get(url);
};
