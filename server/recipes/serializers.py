from django.db import transaction
from django.template.defaultfilters import striptags, truncatechars_html

from rest_framework import serializers

from .models import (
    Category, 
    Food, 
    Ingredient, 
    Recipe, 
    RecipeNote, 
    RecipeReview, 
    RecipeSearchWord, 
    UserRecipe,
)


class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = ('id', 'name',)


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('id', 'description', 'rank', 'is_optional',)


class RecipeSerializer(serializers.ModelSerializer):
    ingredients = IngredientSerializer(many=True, read_only=True)

    class Meta:
        model = Recipe
        fields = (
            'id', 'name', 'description', 'ingredients_text', 'ingredients', 'instructions', 'photo', 'average_make_again',
            'average_rating', 'num_reviews',
        )


class RecipeSearchWordSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeSearchWord
        fields = ('word',)


class RecipeSearchSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    short_description = serializers.SerializerMethodField()

    def get_short_description(self, obj):
        return striptags(truncatechars_html(obj.description, 255))

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'short_description', 'photo', 'average_rating', 'num_reviews',)


class RecipeNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeNote
        fields = ('id', 'recipe', 'user', 'note', 'created_ts', 'updated_ts',)
        read_only_fields = ('id', 'created_ts', 'updated_ts',)


class RecipeReviewSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = RecipeReview
        fields = ('id', 'recipe', 'user', 'make_again', 'rating', 'review', 'username',)
        read_only_fields = ('id', 'username',)

    def create(self, validated_data):
        with transaction.atomic():
            # Create review.
            review = super().create(validated_data)

            # Update recipe.
            recipe = review.recipe
            recipe.total_make_again += (1 if review.make_again else 0)
            recipe.total_ratings += review.rating
            recipe.num_reviews += 1
            recipe.save()

            return review

    def update(self, instance, validated_data):
        with transaction.atomic():
            # Update recipe.
            recipe = instance.recipe
            recipe.total_make_again -= (1 if instance.make_again else 0)
            recipe.total_ratings -= instance.rating
            recipe.num_reviews -= 1

            # Update review.
            review = super().update(instance, validated_data)

            # Update recipe again.
            recipe.total_make_again += (1 if review.make_again else 0)
            recipe.total_ratings += review.rating
            recipe.num_reviews += 1
            recipe.save()

            return review


class ReadUserRecipeSerializer(serializers.ModelSerializer):
    recipe = RecipeSearchSerializer(read_only=True)

    class Meta:
        model = UserRecipe
        fields = ('id', 'user', 'recipe',)


class WriteUserRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserRecipe
        fields = ('id', 'user', 'recipe',)


class UserRecipeSerializer(serializers.ModelSerializer):
    recipe = RecipeSerializer(read_only=True)

    class Meta:
        model = UserRecipe
        fields = ('id', 'user', 'recipe',)


class UserRecipeSearchSerializer(serializers.ModelSerializer):
    recipe = RecipeSearchSerializer()

    def create(self, validated_data):
        recipe = Recipe.objects.get(**validated_data.pop('recipe'))
        return UserRecipe.objects.create(**{'recipe': recipe, **validated_data})

    def update(self, instance, validated_data):
        raise NotImplementedError()

    class Meta:
        model = UserRecipe
        fields = ('id', 'user', 'recipe',)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name',)


class GroceryListItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = ('id', 'name', 'category',)


class GroceryListSerializer(serializers.Serializer):
    categories = CategorySerializer(many=True)
    foods = GroceryListItemSerializer(many=True)
